#![feature(drain_filter)]

use std::fs::File;
use std::io::Write;
use std::net::TcpStream;
use std::rc::Rc;

use chrono::{Datelike, Utc};
use clap::{arg, Parser};
use log::{debug, error, info, LevelFilter};
use log4rs::append::console::ConsoleAppender;
use log4rs::append::file::FileAppender;
use log4rs::Config;
use log4rs::config::{Appender, Root};
use log4rs::encode::pattern::PatternEncoder;
use log4rs::filter::threshold::ThresholdFilter;
use regex::Regex;
use termtree::Tree;

use crate::communication::{check_for_passive, login};
use crate::data_handlers::DataHandlerBuilder;
use crate::directory::{BreadthFirstSearcher, DepthFirstSearcher, FileTree, Searcher, TreeBuilder};
use crate::events::EventsToSend;
use crate::output_handler::{JSONTree, JSONTreeGenerator, PrintableTreeGenerator};
use crate::remote_parser::{BasicRemoteParser, RecursiveRemoteParser};


use crate::tcp_wrapper::TcpWrapper;

mod events;
mod tcp_wrapper;
mod communication;
mod directory;
mod output_handler;
mod remote_parser;
mod data_handlers;

/// A simple to use FTP client to see all the files on your FTP server.
#[derive(Parser, Debug)]
#[command(author, version, about)]
struct Args {
    /// Address to use
    address: String,
    /// Port to use
    #[arg(long, default_value_t = 21)]
    port: u16,
    /// Username to use
    #[arg(short, long)]
    username: Option<String>,
    /// The password you want to use
    #[arg(short, long)]
    password: Option<String>,
    /// Add this option if you want to use the breadth first search algorithm
    #[arg(long)]
    bfs: bool,
    /// Add this option if you want to output the result in a JSON format
    #[arg(long)]
    json: bool,
    /// Add this option if you want to limit the depth of the tree
    #[arg(long)]
    depth: Option<u16>,
    /// Add this option if you want to see debug logs in the console
    #[arg(long)]
    debug: bool,
    /// Timeout for the connection (in seconds)
    #[arg(long, default_value_t = 5)]
    timeout: u64,
    /// How much time you want to retry when the application is failing to get a response from the server.
    #[arg(long, default_value_t = 5)]
    max_retries:u8,
    /// The path to the file you want to output the tree to
    #[arg(short, long)]
    output: Option<String>,
    /// show only directories
    #[arg(short, long)]
    directories: bool,
    /// show permissions
    #[arg(long)]
    permissions: bool,
    /// Show only pattern
    #[arg(short = 'P', long)]
    pattern: Option<Regex>,
    /// Ignore pattern
    #[arg(short = 'I', long)]
    ignore: Option<Regex>,
    /// Sort alphabetically
    #[arg(long)]
    sort: bool,
    /// Sort reverse alphabetically, if sort is set, this option will be ignored
    #[arg(long)]
    sort_reverse: bool,
}

fn main() {
    let args = Args::parse();
    init_logger(args.debug);
    info!("Just started the program at {}.\n", Utc::now());

    match TcpStream::connect(format!("{}:{}", &args.address, &args.port)) {
        Ok(tcp_stream) => {
            debug!("Successfully assigned the TCP Socket to {}.\n", args.address);
            connection_authorized(tcp_stream, args)
        }
        Err(e) => error!("Error when trying to connect to {}. Err: {}", args.address, e)
    }
}

fn connection_authorized(tcp_stream: TcpStream, args: Args) {
    let mut tcp_wrapper = TcpWrapper::new(tcp_stream, Some(std::time::Duration::from_secs(args.timeout)), args.max_retries, &args.address, args.port);
    let r = tcp_wrapper.receive_event();
    info!("{r:?}\n");
    login(&mut tcp_wrapper, args.username, args.password);
    let communicator = check_for_passive(&mut tcp_wrapper);
    let algorithm = if args.bfs {
        info!("Using Breadth First Search algorithm.\n");
        Box::new(BreadthFirstSearcher) as Box<dyn Searcher>
    } else {
        info!("Using Depth First Search algorithm.\n");
        Box::new(DepthFirstSearcher) as Box<dyn Searcher>
    };
    let mut tree_builder = TreeBuilder::new();

    let remote_parser = Box::new(BasicRemoteParser) as Box<dyn RecursiveRemoteParser>;

    let data_handler = DataHandlerBuilder::new()
        .with_depth_limit(args.depth)
        .with_only_directories(args.directories)
        .with_permissions(args.permissions)
        .with_pattern(args.pattern)
        .with_ignore(args.ignore)
        .build();

    tree_builder.get_all_directories_from_remote(algorithm, communicator, &mut tcp_wrapper, remote_parser, data_handler);
    info!("Retrieved all directories from remote.\n");

    let _ = tcp_wrapper.send_and_receive(EventsToSend::Quit);
    
    if args.sort {
        tree_builder.sort_computed_results(|a, b| {
            if a.parent.is_empty() {
                return std::cmp::Ordering::Less;
            } else if b.parent.is_empty() {
                return std::cmp::Ordering::Greater;
            }
            let a = format!("{}", a.item);
            let b = format!("{}", b.item);
            a.cmp(&b)
        });
    } else if args.sort_reverse {
        tree_builder.sort_computed_results(|a, b|{
            if a.parent.is_empty() {
                return std::cmp::Ordering::Less;
            } else if b.parent.is_empty() {
                return std::cmp::Ordering::Greater;
            }
            let a = format!("{}", a.item);
            let b = format!("{}", b.item);
            a.cmp(&b).reverse()
        });
    }


    if args.json {
        info!("Printing the tree in JSON format.\n");
        let generator = Box::new(JSONTreeGenerator);
        let json = tree_builder.build_tree::<JSONTree>(generator);
        print_to_file_or_stdout(serde_json::to_string_pretty(&json).unwrap(), args.output);
    } else {
        info!("Printing the tree in ASCII format.\n");
        let generator = Box::new(PrintableTreeGenerator);
        let tree = tree_builder.build_tree::<Tree<Rc<FileTree>>>(generator);
        print_to_file_or_stdout(tree, args.output);
    }
}

/// Initializing the logger so it writes to a file in `/tmp/tree_ftp`
/// Only the `logging_level` might need to be changed during `dev`.
fn init_logger(debug_std:bool) {
    let now = Utc::now();
    let logging_level = LevelFilter::Debug;
    let logger_error = ConsoleAppender::builder()
        .encoder(Box::new(PatternEncoder::new("=== {l} === {f}:{L} :: {m}")))
        .build();
    let log_path = format!("/tmp/tree_ftp/{}-{:02}-{:02}.log", now.year(), now.month(), now.day());
    let log_file = FileAppender::builder()
        .encoder(Box::new(PatternEncoder::new("{l}: {d(%Y-%m-%d %H:%M:%S)}\t- {f}:{L}\t:: {m}")))
        .build(log_path).unwrap();
    let std = ConsoleAppender::builder()
        .encoder(Box::new(PatternEncoder::new("{l}: {d(%Y-%m-%d %H:%M:%S)} - {f}:{L} :: {m}")))
        .build();
    let config_builder = Config::builder()
        .appender(Appender::builder()
            .build("file_logger", Box::new(log_file)))
        .appender(Appender::builder().build("std", Box::new(std)))
        .appender(Appender::builder()
            .filter(Box::new(ThresholdFilter::new(LevelFilter::Error)))
            .build("error", Box::new(logger_error)));

    let mut root_builder = Root::builder()
        .appender("file_logger")
        .appender("error");
    if debug_std {
        root_builder = root_builder.appender("std");
    }
    let config = config_builder.build(root_builder.build(logging_level)).unwrap();

    let _ = log4rs::init_config(config).unwrap();
}

fn print_to_file_or_stdout<T: std::fmt::Display>(data: T, file_to_output_to:Option<String>) {
    if let Some(file) = file_to_output_to {
        println!("Outputting to file: {}", file);
        let mut file = File::create(file).unwrap();
        file.write_all(data.to_string().as_bytes()).unwrap();
    } else {
        println!("{}", data);
    }
}
