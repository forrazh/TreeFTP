use std::rc::Rc;
use regex::Regex;
use crate::directory::{DirectoryData, FileData, FileTree, Item, TreeBuilder};

pub struct DataHandlerBuilder {
    max_depth: Option<u16>,
    permissions: bool,
    directories: bool,
    pattern: Option<Regex>,
    ignore: Option<Regex>,
}

impl DataHandlerBuilder {
    pub fn new() -> Self {
        DataHandlerBuilder {
            max_depth: None,
            permissions: false,
            directories: false,
            pattern: None,
            ignore: None,
        }
    }

    pub fn with_depth_limit(mut self, depth_limit: Option<u16>) -> Self {
        self.max_depth = depth_limit;
        self
    }

    pub fn with_only_directories(mut self, directories: bool) -> Self {
        self.directories = directories;
        self
    }

    pub fn with_permissions(mut self, permissions: bool) -> Self {
        self.permissions = permissions;
        self
    }

    pub fn with_pattern(mut self, regex: Option<Regex>) -> Self {
        self.pattern = regex;
        self
    }

    pub fn with_ignore(mut self, regex: Option<Regex>) -> Self {
        self.ignore = regex;
        self
    }

    pub fn build(self) -> Box<dyn DataHandler> {
        let mut data_handler: Box<dyn DataHandler>;

        if self.directories {
            data_handler = Box::new(OnlyDirectoriesDataHandler);
        } else {
            data_handler = Box::new(BasicDataHandler);
        }

        if let Some(pattern) = self.pattern {
            data_handler = Box::new(PatternDataHandler::new(data_handler, pattern));
        }

        if let Some(ignore) = self.ignore {
            data_handler = Box::new(IgnoreDataHandler::new(data_handler, ignore));
        }

        if self.permissions {
            data_handler = Box::new(PermissionsDataHandler::new(data_handler));
        }

        if let Some(max_depth) = self.max_depth {
            data_handler = Box::new(DepthLimitedDataHandler::new(max_depth, data_handler));
        }

        data_handler
    }
}

pub struct FileDataHolder {
    display_name: String,
    path: String,
    is_directory: bool,
}

pub trait DataHandler {
    fn handle(&self, file_data:&FileData, path: &str, depth: u16) -> Option<FileDataHolder>;
    fn store_data(&self, tree_builder: &mut TreeBuilder, file_data: &FileData, path: &str, depth: u16) -> Option<Rc<FileTree>> {
        if let Some(file_data_holder) = self.handle(file_data, path, depth) {
            let file = if file_data_holder.is_directory {
                let directory = Rc::new(FileTree::Directory(DirectoryData {
                    depth,
                    name: file_data_holder.display_name,
                    path: file_data_holder.path,
                }));
                tree_builder.directory_queue.push(Item::new(directory.clone(), path.to_string()));
                directory
            } else {
                let file = Rc::new(FileTree::File(file_data_holder.display_name));
                tree_builder.computed_items.push(Item::new(file.clone(), path.to_string()));
                file
            };

            Some(file)
        } else {
            None
        }
    }
}

pub struct BasicDataHandler;

impl DataHandler for BasicDataHandler {
    fn handle(&self, file_data: &FileData, path: &str, _depth: u16) -> Option<FileDataHolder> {
        if file_data.m_rights.starts_with('d') {
            Some(FileDataHolder {
                display_name: file_data.m_name.clone(),
                path: format!("{}/{}", path, file_data.m_name),
                is_directory: true,
            })
        } else {
            Some(FileDataHolder {
                display_name: file_data.m_name.clone(),
                path: format!("{}/{}", path, file_data.m_name),
                is_directory: false,
            })
        }
    }
}

pub struct OnlyDirectoriesDataHandler;

impl DataHandler for OnlyDirectoriesDataHandler {
    fn handle(&self, file_data: &FileData, path: &str, _depth: u16) -> Option<FileDataHolder> {
        if file_data.m_rights.starts_with('d') {
            Some(FileDataHolder {
                display_name: file_data.m_name.clone(),
                path: format!("{}/{}", path, file_data.m_name),
                is_directory: true,
            })
        } else {
            None
        }
    }
}

pub struct PermissionsDataHandler {
    inner_handler: Box<dyn DataHandler>,
}

impl PermissionsDataHandler {
    pub fn new(inner_handler: Box<dyn DataHandler>) -> Self {
        PermissionsDataHandler {
            inner_handler,
        }
    }
}

impl DataHandler for PermissionsDataHandler {
    fn handle(&self, file_data: &FileData, path: &str, depth: u16) -> Option<FileDataHolder> {
        let mut previous_data_holder = self.inner_handler.handle(file_data, path, depth)?;
        previous_data_holder.display_name = format!("{} {}", file_data.m_rights, previous_data_holder.display_name);
        Some(previous_data_holder)
    }
}

pub struct PatternDataHandler {
    inner_handler: Box<dyn DataHandler>,
    pattern: Regex,
}

impl PatternDataHandler {
    pub fn new(inner_handler: Box<dyn DataHandler>, pattern: Regex) -> Self {
        PatternDataHandler {
            inner_handler,
            pattern,
        }
    }
}

impl DataHandler for PatternDataHandler {
    fn handle(&self, file_data: &FileData, path: &str, depth: u16) -> Option<FileDataHolder> {
        if self.pattern.is_match(&file_data.m_name) || self.pattern.is_match(path) {
            self.inner_handler.handle(file_data, path, depth)
        } else {
            None
        }
    }
}

pub struct IgnoreDataHandler {
    inner_handler: Box<dyn DataHandler>,
    ignore: Regex,
}

impl IgnoreDataHandler {
    pub fn new(inner_handler: Box<dyn DataHandler>, ignore: Regex) -> Self {
        IgnoreDataHandler {
            inner_handler,
            ignore,
        }
    }
}

impl DataHandler for IgnoreDataHandler {
    fn handle(&self, file_data: &FileData, path: &str, depth: u16) -> Option<FileDataHolder> {
        if self.ignore.is_match(&file_data.m_name) || self.ignore.is_match(path) {
            None
        } else {
            self.inner_handler.handle(file_data, path, depth)
        }
    }
}

pub struct DepthLimitedDataHandler {
    max_depth: u16,
    inner_handler: Box<dyn DataHandler>,
}

impl DepthLimitedDataHandler {
    pub fn new(max_depth: u16, inner_handler: Box<dyn DataHandler>) -> Self {
        DepthLimitedDataHandler {
            max_depth,
            inner_handler
        }
    }
}

impl DataHandler for DepthLimitedDataHandler {
    fn handle(&self, file_data: &FileData, path: &str, depth: u16) -> Option<FileDataHolder> {
        if depth > self.max_depth {
            None
        } else {
            self.inner_handler.handle(file_data, path, depth)
        }
    }
}
