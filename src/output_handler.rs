use std::rc::Rc;
use log::{debug, trace};
use termtree::Tree;
use serde::Serialize;
use crate::directory::{FileTree, Item};

#[derive(Serialize)]
pub struct JSONTree {
    root: Rc<FileTree>,
    nodes: Vec<JSONTree>,
}

impl JSONTree {
    pub fn new(root: Rc<FileTree>) -> JSONTree {
        JSONTree {
            root,
            nodes: Vec::new(),
        }
    }

    pub fn add_node(&mut self, node: JSONTree) {
        self.nodes.push(node);
    }
}


pub struct PrintableTreeGenerator;
pub struct JSONTreeGenerator;

pub trait OutputGenerator<O> {
    fn init_item(&self, item: Rc<FileTree>) -> O;
    fn push_in_tree(&self, tree: &mut O, node: O);

    /// Fill the tree using the data retrieved from remote in an array.
    fn build_tree(&self, items: &mut Vec<Item>) -> O {
        debug!("Starting to build tree\n");
        let root = items.remove(0);
        let mut tree = self.init_item(root.item.clone());
        self.push_children(items, &mut tree, &root );
        tree
    }

    /// Push the children of a given node in the tree.
    /// This function will recursively call itself until all the children have been added.
    fn push_children(&self, items: &mut Vec<Item>, tree:&mut O, parent: &Item) {
        trace!("Pushing children for {:?}; Still {} items left.", parent, items.len());
        let parent_path = match &*parent.item {
            FileTree::File(_) => return,
            FileTree::Directory(directory_data) => directory_data.path.clone(),
        };
        let children = items.drain_filter(|item| item.parent == parent_path).collect::<Vec<Item>>();
        for child in children {
            let mut child_tree = self.init_item(child.item.clone());
            self.push_children(items, &mut child_tree, &child);
            self.push_in_tree(tree, child_tree);
        }
    }
}

impl OutputGenerator<Tree<Rc<FileTree>>> for PrintableTreeGenerator {
    fn init_item(&self, item: Rc<FileTree>) -> Tree<Rc<FileTree>> {
        Tree::new(item)
    }
    fn push_in_tree(&self, tree: &mut Tree<Rc<FileTree>>, node: Tree<Rc<FileTree>>) {
        tree.push(node);
    }
}


impl OutputGenerator<JSONTree> for JSONTreeGenerator {
    fn init_item(&self, item: Rc<FileTree>) -> JSONTree {
        JSONTree::new(item)
    }
    fn push_in_tree(&self, tree: &mut JSONTree, node: JSONTree) {
        tree.add_node(node);
    }
}
