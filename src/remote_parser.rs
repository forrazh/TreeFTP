use crate::communication::Communicate;
use crate::data_handlers::DataHandler;
use crate::directory::{FileData, FileTree, Item, Searcher, TreeBuilder};
use crate::events::EventsToSend;
use crate::tcp_wrapper::TcpWrapper;

pub struct BasicRemoteParser;

impl RecursiveRemoteParser for BasicRemoteParser {}

pub trait RecursiveRemoteParser {
    fn get_path_and_depth(&self, current_node: &Item) -> Option<(String, u16)> {
        match &*current_node.item {
            FileTree::File(_) => None,
            FileTree::Directory(directory_data) => Some((directory_data.path.clone(), directory_data.depth)),
        }
    }

    fn get_all_directories_from_remote_recursively(&self, tree_builder:&mut TreeBuilder, current_node: Item, searcher: Box<dyn Searcher>, communicator: Box<dyn Communicate>, tcp_wrapper: &mut TcpWrapper, data_handler:Box<dyn DataHandler>) -> Option<()> {
        let (path, depth) = self.get_path_and_depth(&current_node)?;

        let _ = tcp_wrapper.send_and_receive(EventsToSend::Cwd(path.clone()));
        let lines = communicator.retrieve_lines(tcp_wrapper);

        for line in lines {
            let file_data = FileData::new(line);
            let _new_node = data_handler.store_data(tree_builder, &file_data, &path, depth+1);
        }

        tree_builder.computed_items.push(current_node);

        let next = searcher.get_next(&mut tree_builder.directory_queue)?;

        self.get_all_directories_from_remote_recursively(tree_builder, next, searcher, communicator, tcp_wrapper, data_handler);
        Some(())
    }
}
