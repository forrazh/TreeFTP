use std::{io, process};
use std::io::{BufRead, BufReader, BufWriter, Read, Write};
use std::net::{IpAddr, Ipv4Addr, TcpStream};
use std::num::ParseIntError;
use std::str::FromStr;
use std::time::Duration;
use regex::Regex;
use log::{debug, error, info, warn};
use crate::events::{EventsToReceive, EventsToSend, FtpResponseComplementData};


/// Used to send and retrieve messages in our app.
pub struct TcpWrapper {
    stream:TcpStream,
    max_retries:u8,
    address: String,
}

impl TcpWrapper {
    #[cfg(test)]
    pub fn new_only_stream(stream:TcpStream) -> TcpWrapper {
        let address = stream.peer_addr().unwrap().to_string();
        TcpWrapper {
            stream,
            max_retries: 3,
            address,
        }
    }
    pub fn new(stream:TcpStream, timeout:Option<Duration>, max_retries: u8, address: &str, port: u16) -> Self {
        stream.set_read_timeout(timeout).unwrap();
        stream.set_write_timeout(timeout).unwrap();
        Self {
            stream,
            max_retries,
            address:format!("{}:{}", address, port),
        }
    }

    /// Wrapper made to send a message to the server.
    /// Makes the code more readable.
    fn send_buffer(&self, buf: &[u8]) -> io::Result<()> {
        let mut writer = BufWriter::new(&self.stream);
        writer.write_all(buf)?;
        writer.flush()
    }

    /// Implement a send method that can retry to send a message if it fails.
    fn send_failing_event(&mut self, event:&EventsToSend, number_of_retries:u8) -> io::Result<()> {
        if number_of_retries > self.max_retries {
            error!("Failed to send event {} after {} retries. Exiting.\n", event.as_string(), self.max_retries);
            process::exit(1);
        }
        let event_as_str = event.as_string();
        let to_log = if event_as_str.starts_with("PASS"){ "PASS *concealed_password*\n" } else { &event_as_str } ;
        info!("trying to send {to_log}");
        let res = self.send_buffer(event_as_str.as_ref());
        match &res {
            Ok(_) => debug!("Successfully sent {to_log}"),
            Err(err) => {
                error!("{err}");
                return match err.kind() {
                    io::ErrorKind::BrokenPipe => {
                        warn!("Broken pipe. Respawing socket.\n");
                        self.respawn_socket_and_send(event, 0)
                    },
                    _ => {
                        warn!("Retrying...");
                        self.send_failing_event(event, number_of_retries+1)
                    }
                }

            }
        }
        res
    }

    /// Sends an event through the TcpStream, event will be logged as info;
    /// if successful again as debug, if it failed to send it will be as an error.
    fn send_event(&mut self, event:EventsToSend) -> io::Result<()>{
        self.send_failing_event(&event, 0)
    }

    fn receive_failing_event(&self, number_of_retries:u8) -> (EventsToReceive, FtpResponseComplementData) {
        if number_of_retries > self.max_retries {
            error!("Failed to read event after {} retries. Exiting.\n", self.max_retries);
            process::exit(1);
        }
        let mut reader = BufReader::new(&self.stream);
        let mut content: String = "".to_string();
        match reader.read_line(&mut content) {
            Ok(x) => {
                if x == 0 {
                    debug!("Nothing to read here\n");
                    content = "001".to_string();
                } else {
                    debug!("Read {x} chars. CONTENT:: {content}")
                }
            }            Err(err) => {
                error!("{err}\n");
                warn!("Retrying...\n");
                return self.receive_failing_event(number_of_retries + 1)
            }
        }

        let (code_res, opt) = parse_response(reader, &content);
        let code = code_res.unwrap_or_else(|err| {
            error!("{err}");
            000
        });

        (EventsToReceive::get_from_code(code), opt)
    }

    /// Receive an event through the TcpChannel.
    pub fn receive_event(&self) -> (EventsToReceive, FtpResponseComplementData) {
        self.receive_failing_event(0)
    }

    /// Everything is in the name, it should be the one used rather than the classic receive_event as it will always send a response.
    pub fn send_and_receive(&mut self, to_send:EventsToSend) -> (EventsToReceive, FtpResponseComplementData)  {
        let _ = self.send_event(to_send);
        let received = self.receive_event();
        debug!("{received:?}\n");
        received
    }
    fn respawn_socket_and_send(&mut self, event: &EventsToSend, retries: u8) -> io::Result<()> {
        if retries > self.max_retries {
            error!("Failed to respawn socket after {} retries. Exiting.\n", self.max_retries);
            process::exit(1);
        }
        self.stream = TcpStream::connect(&self.address).unwrap();
        let result = self.send_failing_event(event, 0);
        match result {
            Ok(_) => Ok(()),
            Err(err) => {
                error!("{err}\n");
                warn!("Retrying...\n");
                self.respawn_socket_and_send(event, retries + 1)
            }
        }
    }
}

/// Parses an IP address given in the `PASV` response
fn parse_ip_addr_response(response: &str) -> Option<(IpAddr, u16)> {
    let re = Regex::new(r"(\d{1,3},){5}(\d{1,3})").unwrap();
    let captures = re.captures(response)?;
    let mut blocks = captures.get(0)?.as_str().split(',');
    let ip_str = format!(
        "{}.{}.{}.{}",
        blocks.next()?,
        blocks.next()?,
        blocks.next()?,
        blocks.next()?,
    );

    let p1 = blocks.next()?.parse::<u16>().ok()?;
    let p2 = blocks.next()?.parse::<u16>().ok()?;
    let port = (p1 << 8) + p2;
    let ip = Ipv4Addr::from_str(&ip_str).ok()?;
    let ip_addr = IpAddr::V4(ip);
    debug!("Converted {response} into {ip_addr}:{port}\n");
    Some((ip_addr, port))
}

/// Parses the response code and the complement data if any.
fn parse_response<R:Read>(mut reader: BufReader<R>, content: &str) -> (Result<u16, ParseIntError>, FtpResponseComplementData) {
    if content.starts_with("211") {
        let mut content;
        let mut data = Vec::new();
        while {
            content = "".to_string();
            let _ = reader.read_line(&mut content);
            !content.starts_with("211")
        } {
            data.push(content);
        }
        return (Ok(211), FtpResponseComplementData::FeatList(data))
    } else if content.starts_with("227") {
        let (ip_address, port) = parse_ip_addr_response(content).unwrap();
        return (Ok(227), FtpResponseComplementData::IpAddress(ip_address, port));
    }
    (content[..3].parse::<u16>(), FtpResponseComplementData::None)
}

#[cfg(test)]
mod tests {
    use std::net::{IpAddr, Ipv4Addr, SocketAddr, SocketAddrV4, TcpListener};
    use std::sync::atomic::{AtomicUsize, Ordering};
    use std::{env, thread};
    use super::*;

    static PORT: AtomicUsize = AtomicUsize::new(0);

    fn base_port() -> u16 {
        let cwd = if cfg!(target_env = "sgx") {
            String::from("sgx")
        } else {
            env::current_dir().unwrap().into_os_string().into_string().unwrap()
        };
        let dirs = ["32-opt", "32-nopt", "musl-64-opt", "cross-opt", "64-opt", "64-nopt", "64-opt-vg", "64-debug-opt", "all-opt", "snap3", "dist", "sgx", ];
        dirs.iter().enumerate().find(|&(_, dir)| cwd.contains(dir)).map(|p| p.0).unwrap_or(0) as u16
            * 1000
            + 19600
    }

    /// Function taken from the standard lib
    fn next_test_ip4() -> SocketAddr {
        let port = PORT.fetch_add(1, Ordering::SeqCst) as u16 + base_port();
        SocketAddr::V4(SocketAddrV4::new(Ipv4Addr::new(127, 0, 0, 1), port))
    }

    /// Macro taken from the standard lib
    macro_rules! t {
        ($e:expr) => {
            match $e {
                Ok(t) => t,
                Err(e) => panic!("received error for `{}`: {}", stringify!($e), e),
            }
        };
    }

    #[test]
    fn test_tcp_communication() {
        let socket_addr = next_test_ip4();
        let listener = t!(TcpListener::bind(&socket_addr));

        // create a separate thread to handle the incoming client connection
        thread::spawn(move || {
            let (stream, _) = listener.accept().unwrap();
            let cloned_stream = stream.try_clone().unwrap();
            let mut reader = BufReader::new(&cloned_stream);
            let mut client_wrapper = TcpWrapper::new_only_stream(stream);

            // receive event from the other client
            let mut content:String = "".to_string();
            let _ = reader.read_line(&mut content);
            assert_eq!(content, EventsToSend::User(None).as_string());

            // send a response event
            // let event = EventsToReceive::SpecifyPassword;
            let sent = client_wrapper.send_event(EventsToSend::TestEvent("331 Need a password D:".to_string()));
            assert!(sent.is_ok());
        });

        // create the first TcpWrapper struct (simulating the first client)
        let stream = TcpStream::connect(socket_addr).unwrap();
        let mut client1_wrapper = TcpWrapper::new_only_stream(stream);

        // send an event to the other client
        let event = EventsToSend::User(None);
        let sent = client1_wrapper.send_event(event);
        assert!(sent.is_ok());

        // receive a response event
        let (received_event, _) = client1_wrapper.receive_event();
        assert_eq!(received_event, EventsToReceive::SpecifyPassword);
    }

    #[test]
    fn test_parse_ip_addr_response() {
        let response = "Entering Passive Mode (172,18,15,25,195,105)";
        let (ip, port) = parse_ip_addr_response(response).unwrap();
        let expected = IpAddr::V4(Ipv4Addr::from_str("172.18.15.25").unwrap());

        assert_eq!(ip,expected);
        assert_eq!(port, 50025);
    }

    #[test]
    fn test_parse_response() {
        let string_reader = BufReader::new("-rw-r--r--    1 owner    group         1803 Oct  5  20:12 file1.txt\n-rw-r--r--    1 owner    group         1803 Oct  5  20:12 file2.txt\n211 end of status".as_bytes());
        let response = "211-File status okay; about to open data connection.\n";
        let (code, data) = parse_response(string_reader, response);
        assert_eq!(code, Ok(211));
        if let FtpResponseComplementData::FeatList(list) = data {
            assert_eq!(list.len(), 2);
            assert_eq!(list[0], "-rw-r--r--    1 owner    group         1803 Oct  5  20:12 file1.txt\n");
            assert_eq!(list[1], "-rw-r--r--    1 owner    group         1803 Oct  5  20:12 file2.txt\n");
        } else {
            panic!("Should return a file list")
        }

        let response = "227 Entering Passive Mode (172,18,15,25,195,105)";
        let (code, data) = parse_response(BufReader::new("".as_bytes()), response);
        assert_eq!(code, Ok(227));
        if let FtpResponseComplementData::IpAddress(ip, port) = data {
            let expected = IpAddr::V4(Ipv4Addr::from_str("172.18.15.25").unwrap());
            assert_eq!(ip, expected);
            assert_eq!(port, 50025);
        } else {
            panic!("Should return a passive mode")
        }
    }
}