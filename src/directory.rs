use std::cmp::Ordering;
use std::fmt::{Display, Formatter};
use std::rc::Rc;
use serde::Serialize;
use crate::communication::Communicate;
use crate::data_handlers::DataHandler;
use crate::output_handler::OutputGenerator;
use crate::remote_parser::RecursiveRemoteParser;
use crate::tcp_wrapper::TcpWrapper;

#[derive(Clone, Debug, Serialize)]
pub enum FileTree {
    File(String),
    Directory(DirectoryData),
}

impl Display for FileTree {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            FileTree::File(file_name) => write!(f, "{}", file_name),
            FileTree::Directory(directory_data) => write!(f, "{}", directory_data.name),
        }
    }
}

#[derive(Clone, Debug, Serialize)]
pub struct DirectoryData {
    pub(crate) depth: u16,
    pub(crate) name: String,
    pub(crate) path: String,
}

pub struct TreeBuilder {
    pub(crate) computed_items: Vec<Item>,
    pub(crate) directory_queue: Vec<Item>,
}

#[derive(Debug)]
pub struct Item {
    pub item: Rc<FileTree>,
    pub parent: String,
}

impl Item {
    pub(crate) fn new(item: Rc<FileTree>, parent: String) -> Self {
        Item {
            item,
            parent,
        }
    }
}

pub trait Searcher {
    fn get_next(&self, elems: &mut Vec<Item>) -> Option<Item>;
}

pub struct DepthFirstSearcher;

pub struct FileData {
    pub m_rights:     String,
    pub m_hard_links: String,
    pub m_owner:      String,
    pub m_group:      String,
    pub m_size:       String,
    pub m_month:      String,
    pub m_day:        String,
    pub m_year_hour:  String,
    pub m_name:       String,
}

impl FileData {
    pub(crate) fn new(line: String) -> FileData {
        let mut split = line.split_whitespace();

        FileData {
            m_rights: split.next().unwrap().to_string(),
            m_hard_links: split.next().unwrap().to_string(),
            m_owner: split.next().unwrap().to_string(),
            m_group: split.next().unwrap().to_string(),
            m_size: split.next().unwrap().to_string(),
            m_month: split.next().unwrap().to_string(),
            m_day: split.next().unwrap().to_string(),
            m_year_hour: split.next().unwrap().to_string(),
            m_name: split.next().unwrap().to_string(),
        }
    }
}

impl TreeBuilder {
    pub fn new() -> TreeBuilder {
        TreeBuilder {
            directory_queue: Vec::new(),
            computed_items: Vec::new(),
        }
    }

    pub fn get_all_directories_from_remote(&mut self, searcher: Box<dyn Searcher>, communicator: Box<dyn Communicate>, tcp_wrapper: &mut TcpWrapper, remote_parser: Box<dyn RecursiveRemoteParser>, data_handler: Box<dyn DataHandler>) {
        let root_item = Rc::new(FileTree::Directory(DirectoryData {
            depth: 0,
            name: String::from("~"),
            path: String::from("~"),
        }));
        remote_parser.get_all_directories_from_remote_recursively(self, Item::new(Rc::clone(&root_item), "".to_string()), searcher, communicator, tcp_wrapper, data_handler);
    }

    /// Fill the tree using a generic output generator.
    pub fn build_tree<O>(&mut self,
                         output_generator: Box<dyn OutputGenerator<O>>
    ) -> O {
        output_generator.build_tree(&mut self.computed_items,)
    }

    pub(crate) fn sort_computed_results<F>(&mut self, compare: F) where
        F: FnMut(&Item, &Item) -> Ordering{
        self.computed_items.sort_by(compare);
    }
}


impl Searcher for DepthFirstSearcher {
    fn get_next(&self, elems: &mut Vec<Item>) -> Option<Item> {
        elems.pop()
    }
}

pub struct BreadthFirstSearcher;

impl Searcher for BreadthFirstSearcher {
    fn get_next(&self, elems: &mut Vec<Item>) -> Option<Item> {
        if elems.is_empty() {
            None
        } else {
            Some(elems.remove(0))
        }
    }
}

