use std::io::{BufRead, BufReader};
use std::net::TcpStream;
use std::process;
use log::{debug, error, info};
use crate::events::{EventsToReceive, EventsToSend, FtpResponseComplementData};
use crate::tcp_wrapper::TcpWrapper;

pub fn login(tcp_wrapper: &mut TcpWrapper, username:Option<String>, password:Option<String>) {
    let to_send_user = EventsToSend::User(username);
    let to_send_pass = EventsToSend::Pass(password);
    let _ = tcp_wrapper.send_and_receive(to_send_user);
    let (login_event, _) = tcp_wrapper.send_and_receive(to_send_pass);
    if let EventsToReceive::LoginIncorrect = login_event {
        error!("530: Failed to login to the server. Verify your credentials.\n");
        process::exit(9);
    }
}

const ACCEPTED_EVENTS: [&str; 2] = ["PASV", "EPSV"];

pub fn check_for_passive(tcp_wrapper: &mut TcpWrapper) -> Box<dyn Communicate> {
    let received = tcp_wrapper.send_and_receive(EventsToSend::Feat);
    if let (EventsToReceive::SystemStatus, FtpResponseComplementData::FeatList(data))  = received {
        for accepted in ACCEPTED_EVENTS {
            let a = accepted.to_string();
            if data.iter().any(|s| { s.contains(&a) }) {
                debug!("Passive mode available ({a})\n");
                return Box::new(PassiveCommunicator::new(a))
            }
        }
    }
    debug!("Passive mode not available\n");
    Box::new(ActiveCommunicator)
}

pub trait Communicate {
    fn retrieve_lines(&self, tcp_wrapper:&mut TcpWrapper) -> Vec<String>;
}

pub struct PassiveCommunicator{
    pass:String
}

impl PassiveCommunicator {
    pub fn new(pass:String) -> PassiveCommunicator {
        PassiveCommunicator { pass }
    }
}

pub struct ActiveCommunicator;

impl PassiveCommunicator {
    fn deal_with_passive_stream(&self, passive_stream:&mut TcpStream) -> Vec<String> {
        debug!("In PassiveCommunicator\n");
        let mut lines = Vec::new();
        let reader = BufReader::new(passive_stream);
        reader.lines().for_each(|line| {
            if let Ok(line) = line {
                info!("Just received : {:?}\n", line);
                lines.push(line);
            }
        });
        lines
    }
}

impl Communicate for PassiveCommunicator {
    fn retrieve_lines(&self, tcp_wrapper: &mut TcpWrapper) -> Vec<String> {
        let passive_event = self.pass.clone();
        let received = tcp_wrapper.send_and_receive(EventsToSend::Pasv(passive_event));
        let mut lines = Vec::new();
        if let (EventsToReceive::PassiveMode, FtpResponseComplementData::IpAddress(addr, port)) = received {
            debug!("Passive mode activated\n");
            let passive_stream_result = TcpStream::connect(format!("{}:{}", addr, port));
            if let Ok(mut stream) = passive_stream_result {
                let _received_list = tcp_wrapper.send_and_receive(EventsToSend::List(None));
                lines = self.deal_with_passive_stream(&mut stream);
                let _receive = tcp_wrapper.receive_event();
            }
        }
        lines
    }
}

impl Communicate for ActiveCommunicator {
    fn retrieve_lines(&self, _tcp_wrapper: &mut TcpWrapper) -> Vec<String>{
        Vec::new()
    }
}