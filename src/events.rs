use std::net::IpAddr;


/// If in need of other events to send :
/// https://en.wikipedia.org/wiki/List_of_FTP_commands
#[cfg_attr(test, derive(PartialEq, Debug))]
pub enum EventsToSend {
    _Noop,
    User(Option<String>),
    Pass(Option<String>),
    Pasv(String),
    Feat,
    List(Option<String>),
    // Cdup,
    Cwd(String),
    Quit,
    #[cfg(test)]
    TestEvent(String),
}

impl EventsToSend {
    pub fn as_string(&self) -> String {
        match self {
            EventsToSend::_Noop => "NOOP\n".to_string(),
            EventsToSend::User(uname) => {
                match uname {
                    Some(uname) => format!("USER {uname}\n"),
                    None => "USER anonymous\n".to_string(),
                }
            }
            EventsToSend::Pass(pword) => {
                match pword {
                    Some(pword) => format!("PASS {pword}\n"),
                    None => "PASS anonymous\n".to_string()
                }
            }
            EventsToSend::Pasv(s) => format!("{s}\n"),
            EventsToSend::Feat => "FEAT\n".to_string(),
            EventsToSend::List(directory) => {
                match directory {
                    Some(d) => format!("LIST {d}\n"),
                    None => "LIST\n".to_string()
                }
            }
            EventsToSend::Cwd(path) => format!("CWD {path}\n"),
            EventsToSend::Quit => "QUIT\n".to_string(),
            #[cfg(test)]
            EventsToSend::TestEvent(s) => format!("{s}\n"),
            // EventsToSend::Cdup => "CDUP\n".to_string(),
        }
    }
}

/// If in need of other return codes events :
/// https://en.wikipedia.org/wiki/List_of_FTP_server_return_codes
#[derive(Debug, Eq, PartialEq)]
pub enum EventsToReceive {
    NothingForMeHere,
    SuccessfulConnection,
    SystemStatus,
    // add data??
    SpecifyPassword,
    NeedAccountForLogin,
    LoginSuccessful,
    LoginIncorrect,
    DirectoryListing,
    DirectorySuccessfullyChanged,
    FailedDirectoryChange,
    Goodbye,
    PassiveMode,
    UnknownCode,
}

impl EventsToReceive {
    pub fn get_from_code(code: u16) -> EventsToReceive {
        match code {
            001 => EventsToReceive::NothingForMeHere,
            150 => EventsToReceive::DirectoryListing,
            211 => EventsToReceive::SystemStatus,
            220 => EventsToReceive::SuccessfulConnection,
            221 => EventsToReceive::Goodbye,
            227 => EventsToReceive::PassiveMode,
            230 => EventsToReceive::LoginSuccessful,
            250 => EventsToReceive::DirectorySuccessfullyChanged,
            331 => EventsToReceive::SpecifyPassword,
            332 => EventsToReceive::NeedAccountForLogin,
            530 => EventsToReceive::LoginIncorrect,
            550 => EventsToReceive::FailedDirectoryChange,
            _ => EventsToReceive::UnknownCode
        }
    }
}

#[derive(Debug)]
pub enum FtpResponseComplementData {
    FeatList(Vec<String>),
    IpAddress(IpAddr, u16),
    None,
}