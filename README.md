# treeftp

Author: [Hugo Forraz](mailto://hugo@forraz.com)

Language: Rust

This is my version of the `tree` command, but for FTP servers.

While making it I tried to learn a few things about Rust, so it's not perfect, but it works.

You can find a Linux executable at the root of this repo as well as a presenting video.

## Table of Contents <!-- omit in toc -->

- [How to](#how-to)
  - [How to compile/run this project ?](#how-to-compilerun-this-project-)
  - [Shoutouts](#shoutouts)
- [Completed](#completed)
- [implementation](#implementation)
  - [CLI arguments](#cli-arguments)
  - [Logger](#logger)
  - [FTP](#ftp)
  - [Making the actual display](#making-the-actual-display)
  - [JSON](#json)
  - [Max depth](#max-depth)
  - [Tree arguments](#tree-arguments)
- [Make it better](#make-it-better)
  - [Make a better test coverage](#make-a-better-test-coverage)
  - [Make a better usage of Rust](#make-a-better-usage-of-rust)


## How to

### How to compile/run this project ?

You can install the rust toolchain through this tutorial : [https://www.rust-lang.org/tools/install](https://www.rust-lang.org/tools/install)

> Please note that this application uses the **NIGHTLY** version of Rust.

Then, you can compile the project with the following command :

```shell
cargo build --release
```

It will create an executable than you can will find in `target/release` named `treeftp`.

Try running it with the `--help` option to see all the available options (`treeftp --help`).

You can also run the tests using :

```shell
cargo test
```

### Shoutouts

This project would have taken a lot more time to be done without the help of the following crates :
- [clap](https://crates.io/crates/clap) : To make a smooth CLI application
- [log4rs](https://crates.io/crates/log4rs) & [log](https://crates.io/crates/log)  : To log everything that happened in our app
- [termtree](https://crates.io/crates/termtree) : To show the tree in the terminal
- [serde](https://crates.io/crates/serde) & [serde_json](https://crates.io/crates/serde_json) : for the json "serializing" part

## Completed

- MANDATORY
  - [x] Project that compiles...
  - [x] Retrieve the address
  - [x] USER
  - [x] PASS
  - [X] LIST
  - [X] CWD
  - [ ] CDUP
  - [ ] RFC959
- Some more points...
  - [x] optional username / password parameter 
  - [X] optional depth        
  - [X] optional as_json      
  - [X] optional bfs          
  - [X] optional resilience   
  - [X] optional tree parameters

## Implementation

### CLI Arguments

The arguments are handled with the [`Clap` crate](https://crates.io/crates/clap).
Now, here's the generated `help` panel for our executable (when I started this project) :

```shell
$ treeftp --help
A simple to use FTP client to see all the files on your FTP server

Usage: treeftp [OPTIONS] <ADDRESS>

Arguments:
  <ADDRESS>  

Options:
  -u, --username <USERNAME>  Username to use
  -p, --password <PASSWORD>  The password you want to use
  -h, --help                 Print help information
  -V, --version              Print version information
```


### Logger

The logs will be handled by a library called `log4rs`.
When we start the program a file will be created in 
`/tmp/tree_ftp`, for example if today was `January, 6th 2023`,
the file would be named `2023-01-06.log`.

Then we can easily log what happens with the following commands :
- `error`
- `warn`
- `info`
- `debug`
- `trace`

By default, I'll activate only the 3 first but we can change the log level here :
```rs
// main.rs;17:5-17:43;
let logging_level = LevelFilter::Info;
```

Here's an example of some logged messages :

```rs
    info!("Just started the program at {}.", Utc::now());
    info!("Wow, we just received a message from {} !", "TontonHubert");
    debug!("We are just logging some events, nothing to appropriate to say for now...");
    warn!("Hey, something unusual just happened here but it should be okay !");
    error!("WOW, CALM DOWN AND FIX THIS PLEASE !");
```

Which would result in getting the following file :

```log
INFO: 2023-01-06 14:13:45 - src/main.rs:12 :: Just started the program at 2023-01-06 13:13:45
.632668423 UTC.
INFO: 2023-01-06 14:13:45 - src/main.rs:13 :: Wow, we just received a message from TontonHube
rt !
WARN: 2023-01-06 14:13:45 - src/main.rs:15 :: Hey, something unusual just happened here but i
t should be okay !
ERROR: 2023-01-06 14:13:45 - src/main.rs:16 :: WOW, CALM DOWN AND FIX THIS PLEASE !
```

### FTP

FTP being the main point of this project, it has to be done properly.

I implemented a few commands in the `events.rs` file, here's the list of the implemented commands :
- `USER`
- `PASS`
- `LIST`
- `CWD`
- `CDUP`
- `PWD`
- `QUIT`
- `PASV`
- `NOOP`

I also implemented a few things related to the connection in the `tcp_wrapper.rs` file and the `communication.rs` file.

Speaking about the `tcp_wrapper`, you can find the work done to make the application a bit more reliable there.

In this case it was complicated to make something really fancy because we are only working on a single simple client, so I
decided to make a simple retry system that would retry to connect to the server a given number of times before giving up.

The default timeout and number of retries is 5, respectively seconds and attempts.

There was also an issue about the socket sometimes being killed (`ftp.free.fr` was the main culprit) so I tried to make a
simple retry system that would retry to connect to the server a given number of times before giving up.

To sum this up, the fault tolerance supports two things :
- Retrying to send / receive a message when an error occurs
- Retrying to connect to the server when the server kills the client

### Making the actual display

I have been using the following crate to display the tree : [https://crates.io/crates/termtree](https://crates.io/crates/termtree).

It was actually the most difficult part of this project. Displaying it was easy for DFS, as you don't need 
to go back in the tree and just make recursive calls while retrieving every single directory. But while I 
was going onto BFS, I stumbled around some big difficulties.

Filling a tree using DFS or BFS isn't difficult in itself:
- DFS : when a new directory is scanned, you add its children to the queue and directly process them recursively.
  + You can store the children in a FILO data structure like a `Stack`.
- BFS : when a new directory is scanned, you add its children to the queue and process them when you've finished to handle the "brothers" of the current directory.
  + You can store the children in a FIFO data structure like a `Queue`.

But well, the algorithm for the latter requires that you can access the children of a given node in the tree, then modify them...
Which in the case of our library isn't made available by the crate. I tried a few rewrote of my algorithm, tried to fill the tree
in a different way, but nothing worked. 

Here's the thing, Rust being a language *really* focused on safety, it wasn't possible to do this without having access to the 
Tree data structure itself.

Here are the 3 ways that came to my mind to work around this issue :
- Fork the crate and add the feature to it
- Create a new crate that would do what I need it to do
- Fill the tree after computing all the data

I chose the last one, as it was the easiest to implement and the fastest to do but, I'm not really satisfied with the final algorithmic complexity as it needs to recalculate the whole tree afterwards.

You can find the code used for this workaround in the `src/tree.rs` file in the methods `TreeBuilder#build_tree` and `TreeBuilder#push_children`.

Here's a sample output of this program using my session of the `webtp.fil.univ-lille.fr` :

```
~
├── exemples
│   ├── README.md
│   ├── parser
│   │   ├── Makefile
│   │   ├── babel.min.js
│   │   └── index.html
│   └── src
│       ├── 01-expressions-compact.json
│       ├── 01-expressions.js
│       ├── 01-expressions.json
│       └── Makefile
├── iir
│   ├── avatar.md
│   ├── fuzzing_hw_like_sw.md
│   ├── iir-forraz-revol-bauz
│   │   ├── README.md
│   │   └── towards-automated-dynamic-analysis-linux-based-embedded-firmware.pdf
│   └── tp
│       └── tp1.md
├── public_html
├── sr1
│   └── TreeFTP
│       └── treeftp
│           ├── Cargo.lock
│           ├── Cargo.toml
│           ├── README.md
│           ├── src
│           │   ├── communication.rs
│           │   ├── directory.rs
│           │   ├── events.rs
│           │   ├── main.rs
│           │   ├── search.rs
│           │   └── tcp_wrapper.rs
│           └── target
│               ├── debug
│               └── release
└── test
    ├── aaaa
    │   └── documents
    │       ├── 01.introduction.pdf
    │       ├── 06.timed_automata.pdf
    │       ├── 07.comp_architecture.pdf
    │       ├── README.md
    │       ├── training.pdf
    │       ├── uppaal64-4.1.26-2.zip
    │       ├── uppaal_new-tutorial.pdf
    │       └── uppaal_small_tutorial.pdf
    ├── bbbb
    │   └── Question3.png
    └── cccc
        └── examples
            ├── CMakeLists.txt
            ├── ac
            │   ├── CMakeLists.txt
            │   └── ac.cpp
            ├── coffee
            │   ├── CMakeLists.txt
            │   ├── coffee.cpp
            │   ├── coffee.org
            │   └── coffee_machine.png
            ├── fan
            │   ├── CMakeLists.txt
            │   └── fan.cpp
            └── hierarchy
                ├── CMakeLists.txt
                ├── hierarchy.cpp
                ├── hierarchy.org
                └── hierarchy.png
```

### JSON

Another thing I underestimated was the JSON output. Adding the `serde` crate to the project was easy, 
but I didn't think about the fact that I would need `termtree` to also implement `serde` to be able to
serialize it. Forking the repo would have been definitely easier and cleaner... 

The first version I made was to just rewrite the methods that build the tree but... It results in 
creating boilerplate code that might need to be updated twice as the project would grow. 

As Rust doesn't allow struct inheritance, the easiest solution to avoid this boilerplate code are the following :
- generify my code and create an enum that handles dispatching correctly.
  - I don't like this solution because of the 2 following points: 
    - it would result in a slight performance drop as I'd need to dispatch every call between every enum variant
    - if I need to add a new variant, I need to update a lot of code
- handle this new output generator in a trait and give it a generic parameter.

I chose the second solution, as it's the most flexible one. I created a new trait `OutputGenerator` that will be able to dispatch the calls to the correct sub-trait.

You can find the code in the [`output_handler.rs`](src/output_handler.rs) file.

To use this kind of output, use the `--json` optional flag, and it will be output as a "pretty printed JSON".

### Max depth

The max depth feature has been implemented as a decorator pattern over the `DataHandler` trait. 
It has been implemented this way for 2 main purposes :
- Make the next arguments handling easier
- simplify how the max depth is handled

### Tree arguments

Here are the arguments recoded of the `tree` command :

- `-o` : output the tree in a file instead of the standard output
- `-d` : display the directories only
- `--permissions` : display the permissions of the files 
- `-P` : display every file with the given pattern appearing in its name or path
- `-I` : display every file that doesn't match the given pattern appearing in its name or path
  + Note that the pattern is a `regex` and that special characters should be escaped with a "\\".
- `--sort` : sort the files and directories by name
- `--sort-reverse` : reverse sort the files and directories by name

## Make it better

Here are some things that I would have liked to do but that I didn't have the time to do :

#### Make a better test coverage

I'm not that used to writing tests in Rust, and I wasn't sure what was relevant to test, so it was a bit difficult to make a good test coverage.

#### Make a better usage of Rust

I've mostly learned using Rust alone and this was the third time I was on a project using it... So my code can be a bit messy and not idiomatic and A LOT of things can be improved.

For example, I could have made a better use of the `Result` type to handle errors more explicitly (here, I mostly suppose that everything will work and that I don't need to handle errors outside of the tcp_wrapper).

